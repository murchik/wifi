import shutil

import wifi.subprocess_compat as subprocess
from wifi import exceptions


def is_available():
    """
    Checks if `netctl` can be executed.
    """
    return bool(shutil.which('netctl'))


def connect(profile):
    """
    Connects to the network specified by profile name.
    """
    subprocess.check_output(['netctl', 'stop-all'], stderr=subprocess.STDOUT)

    try:
        subprocess.check_output(
            ['netctl', 'start', profile], stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError:
        raise exceptions.ConnectionError(
            "Failed to connect to {profile}: {output}")

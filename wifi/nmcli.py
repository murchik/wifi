import shutil

import wifi.subprocess_compat as subprocess
from wifi import exceptions


def is_available():
    """
    Checks if `nmcli` can be executed.
    """
    return bool(shutil.which('nmcli'))


def connect(essid, passkey):
    """
    Connects to the network.
    """
    command = ['nmcli', 'device', 'wifi', 'connect', essid]
    if passkey is not None:
        command += ['password', passkey]

    try:
        subprocess.check_output(command, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError:
        raise exceptions.ConnectionError(
            'Failed to connect to {}'.format(essid))
